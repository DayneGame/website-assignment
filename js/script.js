
// This is adds up all the correct validations. If it has a vlaue of 4 a function sends a message back to the user.
var counter = 0;

//========================================== Navbar Function ========================================================//
function navbarFunction() {                  
    var x = document.getElementById("navbar");
    if (x.className === "nav") {
        x.className += " responsive";
    } else {
        x.className = "nav";
    }
}
//===================================================================================================================//

//========================================== Form Validations =======================================================//

//All of these variable link to the input and textarea boxes.
let firstname = document.querySelector('#fname')
let lastname = document.querySelector('#lname')
let email = document.querySelector('#email')
let subject1 = document.querySelector('#subject')
let messagebox1 = document.querySelector('#message-box')

//All these variables link to span ids in the html file
let nameerror = document.querySelector('#name-error')
let emailerror = document.querySelector('#email-error')
let subjecterror = document.querySelector('#subject-error')
let messageerror = document.querySelector('#message-error')

//This function checks if the user as entered a value in Firstname and Lastname
let validateName = () => {
    counter = 0;// This gives the value of 0 to number so if someone enters an error it resets back to 0
    if (firstname.value == "" || lastname.value == "" ) {
        nameerror.innerHTML = " Please enter your name"
        nameerror.style.color = "red";
        counter = 0;
    }
    else {
        nameerror.innerHTML = "";
        // Adds one to counter
        counter += 1;
        console.log(counter);
    }
    
}

//This function checks to see if the user has entered an email
let validateEmail = () => {
    if (email.value == "") {
        emailerror.innerHTML = " Please enter Email"
        emailerror.style.color = "red";
        counter = 0;
    }
    else {
        //This checks to see if it is a vaild email
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.value))
        {
            emailerror.innerHTML = "";
            counter += 1;
            return (true)
            
        }
        emailerror.innerHTML = " Please enter a valid Email"
        emailerror.style.color = "red";
        counter = 0;
        return(false)
    }
}

//This functions checks if the user has entered a subject
let validateSubject = () => {
    if (subject1.value == "") {
        subjecterror.innerHTML = " Please enter your Subject"
        subjecterror.style.color = "red";
        counter = 0;
    }
    else {
        subjecterror.innerHTML = "";
        // adds one to counter
        counter += 1;
    }
}

//This function checks if the user has entered a message
let validateMessage = () => {
    if (messagebox1.value == "") {
        messageerror.innerHTML = " Please enter your Message"
        messageerror.style.color = "red";
        counter = 0;
    }
    else {
        messageerror.innerHTML = "";
        // adds one to counter
        counter += 1;
    }
}

// This function checks to see if the are validations have been entered correctly
let checkValid = () => {
    // When counter has a value of 4 an alert message pops up to the user to says the message has been sent.
    if (counter == 4) {
        alert("Your message has been sent!");
        console.log(counter);
    }
}

//When the button is clicked it deploys these four functions.
submit1.addEventListener('click', (e) => {
    validateName();
    validateEmail();
    validateSubject();
    validateMessage();
    checkValid();

    e.preventDefault();
})
//===================================================================================================================//


